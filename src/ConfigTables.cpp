#include "../include/ConfigTables.h"

void _eatComment(FILE* fp){
	char str[100];
	fscanf(fp, "\n//");
	while(str[0] != '\n') fscanf(fp, "%c", str);
}



int ConfigTables::Load(const char* path){
	FILE* fp = fopen(path, "r");

	if(fp){
		// eat comment
		_eatComment(fp);

		// load the tri counts
		for(int i = 0; i < 256; i++){	
			int count = 0;
			fscanf(fp, "%d", &count);
			TriCount[i] = count;
#ifdef CONFIG_TABLES_DEBUG
			printf("%d ", count);
#endif
		}
#ifdef CONFIG_TABLES_DEBUG
		printf("\n\n");
#endif
	
		// eat the next comment
		_eatComment(fp);

		// load the triangle lists
		for(int i = 0; i < 256; i++){
			for(int j = 0; j < 15; j++){
				int ti = 0; // tri index
				fscanf(fp, "%d", &ti);
				TriLists[i][j] = ti;
#ifdef CONFIG_TABLES_DEBUG
				printf("%d ", TriLists[i][j]);
#endif
			}
#ifdef CONFIG_TABLES_DEBUG
			printf("\n");
#endif
		}
	
		fclose(fp);
		return 1;
	}
	
	// error opening file
	return 0;
}
