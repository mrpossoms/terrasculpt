#include "../../include/UI/MousePicker.h"

MousePicker::MousePicker(int screenWidth, int screenHeight, SculptCamera* camera){
	_camera = camera;
	_width = screenWidth;
	_height = screenHeight;
	_halfWidth= _width / 2; _halfHeight = _height / 2;
}

vec3 MousePicker::UnProject(int x, int y, mat4* view, mat4* proj){

	GLint viw[] = {
		0, 0, _width, _height
	};

	glGetIntegerv( GL_VIEWPORT, viw );

	mat4 v = *view;
	GLdouble mv[] = {
		v[0].x, v[1].x, v[2].x, v[3].x,
		v[0].y, v[1].y, v[2].y, v[3].y,
		v[0].z, v[1].z, v[2].z, v[3].z,
		v[0].w, v[1].w, v[2].w, v[3].w
	};

	mat4 p = *proj;
	GLdouble prj[] = {
		p[0].x, p[1].x, p[2].x, p[3].x,
		p[0].y, p[1].y, p[2].y, p[3].y,
		p[0].z, p[1].z, p[2].z, p[3].z,
		p[0].w, p[1].w, p[2].w, p[3].w
	};

	//std::cout << "View Matrix " << v << "\nProj Matrix " << p << "\n";

	GLdouble osx, osy, osz;
	GLfloat depth = 0;
	glReadPixels(x, viw[3] - y, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &depth);

	gluUnProject(
		(double)x, (double)(viw[3] - y), (double)depth,
		mv,
		prj,
		viw,
		&osx, &osy, &osz
	);

	return vec3((float)osx, (float)osy, (float)osz);
}

Ray MousePicker::GetPickingRay(int x, int y){

	// calculate canonical x and y [-1, 1]
	/*x -= _halfWidth; y -= _halfHeight;
	float canX = x / (float)_halfWidth;
	float canY = -y / (float)_halfHeight;

	std::cout << "Mx " << canX << " My " << canY << " X " << x << " Y " << y << "\n";

	vec3 left = _camera->GetLeft() * canX * (640.0/480.0) * 2;
	vec3 up = _camera->GetUp() * canY * (640.0/480.0);

	vec3 rayDir = normalize(left + up + _camera->GetForward());*/


	vec3 rayDir = normalize(UnProject(x, y, _camera->GetView(), _camera->GetProj()) - _camera->GetPosition());

	Ray r = {
		_camera->GetPosition(),
		rayDir
	};

	return r;
}