#include "../include/Brushes/SphereBrush.h"

SphereBrush::SphereBrush(float radius, float step) : Brush(radius){ 
	_min = vec3(-radius);
	_max = vec3(radius);
	_step = step;

	_interval = _max - _min;
	_iterations = (int)ceil(_interval.x / (float)_step);
	_interval /= (float)_iterations;
}

void SphereBrush::Extrude(Volume* volume, vec3 Coord, float delta){
	vec3 c  = volume->GetCenter();
	vec3 mn = c + _min, mx = c + _max;

	for(int i = _iterations; i--;)
		for(int j = _iterations; j--;)
			for(int k = _iterations; k--;){
				//std::cout << i << " " << j << " " << k << "\n";

				vec3 off(
					i / (float)_iterations,
					j / (float)_iterations,
					k / (float)_iterations
				);

				vec3 ws = (mn * off) + ((vec3(1) - off) * mx);
				float l = length(ws - c);
				float weight = (_radius - l);
				float current = volume->Density->Sample(ws);

				if(weight > current)
				volume->Density->Update(ws, weight);

			}

	RECONSTRUCT(volume, _iterations);
	//std::cout << "Done!\n"; 

}

void SphereBrush::Carve(Volume* volume, vec3 Coord, float delta){
	vec3 c  = volume->GetCenter();
	vec3 mn = c + _min, mx = c + _max;

	for(int i = _iterations; i--;)
		for(int j = _iterations; j--;)
			for(int k = _iterations; k--;){
				//std::cout << i << " " << j << " " << k << "\n";

				vec3 off(
					i / (float)_iterations,
					j / (float)_iterations,
					k / (float)_iterations
				);

				vec3 ws = (mn * off) + ((vec3(1) - off) * mx);
				float l = length(ws - c);
				float weight = (l - _radius);
				float current = volume->Density->Sample(ws);

				if(weight < current)
				volume->Density->Update(ws, weight);
			}

	RECONSTRUCT(volume, _iterations);
	//std::cout << "Done!\n"; 
}

void SphereBrush::Smooth(Volume* volume, vec3 Coord, float delta){
	return;
}
void SphereBrush::Flatten(Volume* volume, vec3 Coord, float delta){
	return;
}

void SphereBrush::IncreaseRadius(float delta){
	_min = vec3(-(_radius+=delta));
	_max = vec3(_radius);

	_interval = _max - _min;
	_iterations = (int)ceil(_interval.x / (float)_step);
	_interval /= (float)_iterations;
}
void SphereBrush::DecreaseRadius(float delta){
	_min = vec3(-(_radius-=delta));
	_max = vec3(_radius);

	_interval = _max - _min;
	_iterations = (int)ceil(_interval.x / (float)_step);
	_interval /= (float)_iterations;
}