#include "../include/Brushes/BubbleBrush.h"

BubbleBrush::BubbleBrush(float radius, float step) : Brush(radius){ 
	_min = vec3(-radius);
	_max = vec3(radius);
	_step = step;

	_interval = _max - _min;
	_iterations = (int)ceil(_interval.x / _step);
	_interval /= (float)_iterations;
}

int BubbleBrush::isAdjacent(Volume* volume, int x, int y, int z){
	//DensityTexture* dt = (DensityTexture*)volume->Density;
	unsigned char*** texture = volume->Density->Get3DTexture();
	int res = volume->Density->Resolution();
	//printf("Res: %d\n", res);

	for(int i = -1; i <= 1; i++)
		for(int j = -1; j <= 1; j++)
			for(int k = -1; k <= 1; k++){
				if(i + j + k != 0)
				if(i+x >= 0 && i+x < res && j+y >= 0 && j+y < res && k+z >= 0 && k+z < res){
					//printf("%d, %d, %d --> %u\n", i+x, j+y, k+z, texture[x+i][y+j][z+k]);

					if((texture[x+i][y+j][z+k]) == 0xFF)
						return true;
				}

			}
	return false;
}

void BubbleBrush::Extrude(Volume* volume, vec3 Coord, float delta){
	vec3 c  = volume->GetCenter();
	vec3 mn = c + _min, mx = c + _max;
	int res = volume->Density->Resolution();

	int x = (int)(c.x * res);
	int y = (int)(c.y * res);
	int z = (int)(c.z * res);

	for(int i = _iterations; i--;)
		for(int j = _iterations; j--;)
			for(int k = _iterations; k--;){
				//std::cout << i << " " << j << " " << k << "\n";

				vec3 off(
					i / (float)_iterations,
					j / (float)_iterations,
					k / (float)_iterations
				);

				vec3 ws = (mn * off) + ((vec3(1) - off) * mx);
				float current = volume->Density->Sample(ws);
				float l = length(ws - c);
				float weight = (_radius - l);

				if(weight > 0 && (current < 1 || isAdjacent(volume, x + i, y + j, z + k)))
					volume->Density->Increment(ws, delta * (weight / _radius));

			}

	RECONSTRUCT(volume, _iterations/2);
	//std::cout << "Done!\n"; 

}

void BubbleBrush::Carve(Volume* volume, vec3 Coord, float delta){
	vec3 c  = volume->GetCenter();
	vec3 mn = c + _min, mx = c + _max;
	int res = volume->Density->Resolution();

	int x = (int)(c.x * res);
	int y = (int)(c.y * res);
	int z = (int)(c.z * res);

	for(int i = _iterations; i--;)
		for(int j = _iterations; j--;)
			for(int k = _iterations; k--;){
				//std::cout << i << " " << j << " " << k << "\n";

				vec3 off(
					i / (float)_iterations,
					j / (float)_iterations,
					k / (float)_iterations
				);

				vec3 ws = (mn * off) + ((vec3(1) - off) * mx);
				float current = volume->Density->Sample(ws);
				float l = length(ws - c);
				float weight = (_radius - l);

				if(weight > 0 && (current > 0 || isAdjacent(volume, x + i, y + j, z + k)))
					volume->Density->Increment(ws, -delta * ((_radius - l) / _radius));

			}

	RECONSTRUCT(volume, _iterations/2);
}

void BubbleBrush::Smooth(Volume* volume, vec3 Coord, float delta){
	return;
}
void BubbleBrush::Flatten(Volume* volume, vec3 Coord, float delta){
	return;
}

void BubbleBrush::IncreaseRadius(float delta){
	_min = vec3(-(_radius+=delta));
	_max = vec3(_radius);

	_interval = _max - _min;
	_iterations = (int)ceil(_interval.x / _step);
	_interval /= (float)_iterations;
}
void BubbleBrush::DecreaseRadius(float delta){
	_min = vec3(-(_radius-=delta));
	_max = vec3(_radius);

	_interval = _max - _min;
	_iterations = (int)ceil(_interval.x / _step);
	_interval /= (float)_iterations;
}
