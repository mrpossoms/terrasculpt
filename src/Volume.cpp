#include "../include/Volume.h"

ConfigTables* CTconfig;
int EdgeList[12][2] = {
        {0, 1}, // e0
        {1, 2}, // e1
        {2, 3}, // e2
        {3, 0}, // e3
        {4, 5}, // e4
        {5, 6}, // e5
        {6, 7}, // e6
        {4, 7}, // e7
        {0, 4}, // e8
        {1, 5}, // e9
        {2, 6}, // e10
        {3, 7}  // e11
};

//TODO
#define SHMEA 0.03125f

vec3 Offsets[8] = {
	vec3(-SHMEA),
	vec3(-SHMEA, SHMEA, -SHMEA),
	vec3(SHMEA, SHMEA, -SHMEA),
	vec3(SHMEA, -SHMEA, -SHMEA),
	vec3(-SHMEA, -SHMEA, SHMEA),
	vec3(-SHMEA, SHMEA, SHMEA),
	vec3(SHMEA),
	vec3(SHMEA, -SHMEA, SHMEA)
};

Volume::Volume(Volume* parent, vec3 min, vec3 max){
	Parent = parent;

	Max = max; Min = min;

	_center = (min + max) / 2;
	FrameColor = vec3(1);

	// make sure the min and max are actually
	// the min and the max
	for(int i = 0; i < 3; i++){
		if(Max[i] < Min[i]){
			float temp = Max[i];
			Max[i] = Min[i];
			Min[i] = temp;
		}
	}

	if(parent){
		Density = parent->Density;
		Leaves = parent->Leaves;
	}
	else
		Leaves = NULL;

	TriCount = _vboTriCount = 0;
	StartIndex = -1;
	_configStale = true;
	_config = 0;

	HasChildren = false;
	for(int i = 8; i--; Children[i] = NULL);
	
	_corners[0] = min;
	_corners[1] = vec3(min.x, max.y, min.z);
	_corners[2] = vec3(max.x, max.y, min.z);
	_corners[3] = vec3(max.x, min.y, min.z);
	_corners[4] = vec3(min.x, min.y, max.z);
	_corners[5] = vec3(min.x, max.y, max.z);
	_corners[6] = max;
	_corners[7] = vec3(max.x, min.y, max.z);

	// create the wireframe cube
	{
		glGenBuffers(1, &_wireFrame);
		glBindBuffer(GL_ARRAY_BUFFER, _wireFrame);

		vec3 _cubeVerts[20] = {
			_corners[0], _corners[1], _corners[2], _corners[3], _corners[0],
			_corners[4], _corners[5], _corners[1], _corners[0], _corners[4],
			_corners[7], _corners[6], _corners[5], _corners[4], _corners[7],
			_corners[6], _corners[2], _corners[3], _corners[7], _corners[6],
		};

		glBufferData(GL_ARRAY_BUFFER, sizeof(vec3) * 20, _cubeVerts, GL_STATIC_DRAW);
	}

	glGenBuffers(1, &_vertexBuffer);
}
/*---------------------------------------------------------------------------*/
Volume** Volume::Subdivide(vec3 center){
	if(Parent == NULL && Leaves == NULL){
		// Construct the quick lookup map
		int res = Density->Resolution() + 1;
		Leaves = new Volume***[res];
		for(int i = 0; i < res; i++){
			Leaves[i] = new Volume**[res];

			for(int j = 0; j < res; j++)
				Leaves[i][j] = new Volume*[res];
		}
	}

	// create all the children
	for(int i = 8; i--;){
		Children[i] = new Volume(this, _corners[i], center);
	}
	HasChildren = true;

	return Children;
}
/*---------------------------------------------------------------------------*/
Volume** Volume::Subdivide(){
	if(Children[0]){
		for(int i = 8; i--;){
			Children[i]->Subdivide();
			Children[i]->Density = Density;
		}
	}
	else{
		vec3 center = (Min + Max) / 2.0f;
		Subdivide(center);
	}

	return Children;
}
/*---------------------------------------------------------------------------*/
void Volume::SubdivideAndGenTris(int depth, int *VertexIndex){
	// check to see if the volume is inside the mesh
	// if it is, ignore it
	//if(GetConfig() == 0xFF) return;

	if(depth > 0){
		if(Children[0] == NULL){
			Subdivide(); // generate children		
		}

		for(int i = 8; i--;)
			Children[i]->SubdivideAndGenTris(depth - 1, VertexIndex);

		//UpdateVBO(depth);
	}
	else{
		unsigned char c = GetConfig();
		if(c != 0x00 && c != 0xFF){
			GenTris(c);
		}

		// record the range of verts in the index buffer that
		// this vertex will own
		StartIndex = *VertexIndex;
		*VertexIndex += 15;

		// insert self into the Leaves map
		int res = Density->Resolution() + 1;
		Volume* r = GetRoot();
		vec3 diff = (r->Max - r->Min);
		vec3 coord = (_center - r->Min) / diff;
		int x = VolIndex.x = (int)(coord.x * res);
		int y = VolIndex.y = (int)(coord.y * res);
		int z = VolIndex.z = (int)(coord.z * res);
		Leaves[x][y][z] = this;
	}
}
/*---------------------------------------------------------------------------*/
unsigned char Volume::GetConfig(){

	//if(_configStale == true){
		_config = 0x00;

		for(int i = 8; i--;){
			float d = Density->Sample(_corners[i]);
			if(d > 0)
				_config |= (0x01 << i);
		}
			//std::cout << "\n";
		_configStale = false;
	//}

	return _config;
}
/*---------------------------------------------------------------------------*/
vec3 Volume::SolveVert(int ei){
	vec3 v1 = GetCorner(EdgeList[ei][0]);
	vec3 v2 = GetCorner(EdgeList[ei][1]);
	vec3 out(0);

// write(1, "Solving vertex...", 17);
	float s1 = Density->Sample(v1);// + Offsets[EdgeList[ei][0]]);
	float s2 = Density->Sample(v2);// + Offsets[EdgeList[ei][1]]);
	float w1 = s1 <= 0 ? s1 : s2; // negative
	float w2 = s2 > 0 ? s2 : s1; // positive

	//std::cout << "Density @ " << v1 << " = " << s1 << "\n";
	//std::cout << "Density @ " << v2 << " = " << s2 << "\n";

	float p = -w1/(w2 - w1);

	if(s1 > 0){
		out = (v1 * p) + (v2 * (1-p));
	}
	else{
		out = (v2 * p) + (v1 * (1-p));
	}
// write(1, "SV OK!\n", 8);
	return out;
}
/*---------------------------------------------------------------------------*/
void Volume::GenTris(unsigned char config){
	TriCount = CTconfig->TriCount[config];

	bzero(Verts, sizeof(Vertex) * 15);

	for(int i = 0; i < TriCount; ++i){
		for(int j = 0; j < 3; ++j){
			// solve for the vertex location on the edge
			vec3 v = SolveVert(CTconfig->TriLists[config][i * 3 + j]);

			float d = Volume::DELTACELL;
			float w = Density->Sample(v);
			vec3 normal(0);

			//std::cout << "\nStart normal\n";
			normal.x = (w + Density->Sample(v + vec3(d, 0, 0)) / 2.0f) -
			           (w + Density->Sample(v - vec3(d, 0, 0)) / 2.0f);
			normal.y = (w + Density->Sample(v + vec3(0, d, 0)) / 2.0f) -
			           (w + Density->Sample(v - vec3(0, d, 0)) / 2.0f);
			normal.z = (w + Density->Sample(v + vec3(0, 0, d)) / 2.0f) - 
					   (w + Density->Sample(v - vec3(0, 0, d)) / 2.0f);			

			normal = -normalize(normal);

			vec3 blendWeights = vec3(abs(normal.x), abs(normal.y), abs(normal.z));
			blendWeights = (blendWeights - vec3(0.2)) * 7;  
			blendWeights = vec3(
				blendWeights.x < 0 ? 0 : blendWeights.x,
				blendWeights.y < 0 ? 0 : blendWeights.y,
				blendWeights.z < 0 ? 0 : blendWeights.z
			);

			blendWeights /= (blendWeights.x + blendWeights.y + blendWeights.z);

	//std::cout << "Normal @ " << v << " = " << normal << "\n\n";
			Vertex vert = {
				v,
				normal,
				//vec2(0),
				//{0, 0},
				//0.0f
			};

			Verts[i * 3 + j] = vert;
		}
	}
}
/*---------------------------------------------------------------------------*/
void Volume::GenTrisRecursive(){

	if(Children[0] == NULL){
		GenTris(this->GetConfig());
	}
	else{
		for(int i = 8; i--;){
			Children[i]->GenTrisRecursive();
		}
	}
}
/*---------------------------------------------------------------------------*/
Volume* Volume::GetContainingVolume(vec3 pos){
	if(CONTAINS(pos)){
		if(HasChildren)
			for(int i = 8; i--;){
				Volume* v = Children[i]->GetContainingVolume(pos);
				if(v) return v;
			}
		else
			return this;
	}

	return NULL;
}
/*---------------------------------------------------------------------------*/
vec3 Volume::GetCorner(int index){
	return _corners[index];
}
/*---------------------------------------------------------------------------*/
vec3 Volume::GetNearestCorner(Ray ray){
	float det = 0;
	vec3 nearest(0);

	for(int i = 8; i--;){
		float d = dot(_corners[i] - _center, ray.Direction);
		if(d < det){
			nearest = _corners[i];
			det = d;
		}
	}

	return nearest;
}
/*---------------------------------------------------------------------------*/
vec3 Volume::GetCenter(){
	return _center;
}
/*---------------------------------------------------------------------------*/
void Volume::UpdateVBO(int depth){
	int triCount = GetRecursiveTriCount(depth);
	int buffLen = triCount * 3 * sizeof(Vertex);

	std::cout << "Total tris: " << triCount << "\n";

	Vertex* buffer = new Vertex[triCount * 3];
	CopyTris(buffer, depth);

	_vboTriCount = triCount;


	//glDeleteBuffers(1, &_vertexBuffer);

	//glGenBuffers(1, &_vertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, _vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, buffLen, buffer, GL_DYNAMIC_DRAW);

	delete buffer;
}
/*---------------------------------------------------------------------------*/
void Volume::BindVBO(){
	glBindBuffer(GL_ARRAY_BUFFER, _vertexBuffer);
}
/*---------------------------------------------------------------------------*/
void Volume::UpdateVBO(int offset, Vertex* buffer){
	glBufferSubData(GL_ARRAY_BUFFER, offset * sizeof(Vertex), 15 * sizeof(Vertex), buffer);
}
/*---------------------------------------------------------------------------*/
void Volume::BindVolume(GLuint program, GLuint xTexture, GLuint yTexture, GLuint zTexture){
	glUseProgram(program);
	glBindBuffer(GL_ARRAY_BUFFER, _vertexBuffer);

// Initialize the vertex position attribute from the vertex shader
	GLuint aPosition = glGetAttribLocation( program, "aPosition" );
        GLuint aNormal   = glGetAttribLocation( program, "aNormal" );
        GLuint aTexCoord = glGetAttribLocation( program, "aTexCoord" );
        GLuint aSrcTex0  = glGetAttribLocation( program, "aSrcTex0" );
        GLuint aSrcTex1  = glGetAttribLocation( program, "aSrcTex1" );
        GLuint aTexWeight = glGetAttribLocation( program, "aTexWeight" );

	glEnableVertexAttribArray(aPosition);
        glEnableVertexAttribArray(aNormal);
        glEnableVertexAttribArray(aTexCoord);
        glEnableVertexAttribArray(aSrcTex0);
        glEnableVertexAttribArray(aSrcTex1);
        glEnableVertexAttribArray(aTexWeight);

	glVertexAttribPointer(aPosition,  3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (char*)NULL + 0);
        glVertexAttribPointer(aNormal,    3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (char*)NULL + 12);
        glVertexAttribPointer(aTexCoord,  2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (char*)NULL + 24);
        glVertexAttribPointer(aSrcTex0,   1, GL_INT, GL_FALSE, sizeof(Vertex), (char*)NULL + 32);
        glVertexAttribPointer(aSrcTex1,   1, GL_INT, GL_FALSE, sizeof(Vertex), (char*)NULL + 36);
        glVertexAttribPointer(aTexWeight, 1, GL_FLOAT, GL_FALSE, sizeof(Vertex), (char*)NULL + 40);


	_uModel = glGetUniformLocation(program, "uModel");
	_uView = glGetUniformLocation(program, "uView");
	_uProj = glGetUniformLocation(program, "uProj");
	_uColor = glGetUniformLocation(program, "uColor");

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, xTexture);
	glUniform1i(glGetUniformLocation(program, "uXtexture"), 0);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, yTexture);
	glUniform1i(glGetUniformLocation(program, "uYtexture"), 1);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, zTexture);
	glUniform1i(glGetUniformLocation(program, "uZtexture"), 2);
}
/*---------------------------------------------------------------------------*/
void Volume::DrawVolume(mat4 model, mat4 view, mat4 proj){
	//glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
	if(_vboTriCount > 0){
		glUniformMatrix4fv(_uModel, 1, GL_TRUE, (GLfloat*)&model);
		glUniformMatrix4fv(_uView, 1, GL_TRUE, (GLfloat*)&view);
		glUniformMatrix4fv(_uProj, 1, GL_TRUE, (GLfloat*)&proj);
		glDrawArrays(GL_TRIANGLES, 0, _vboTriCount * 3);
	}

	{
		glUseProgram(GLRender_LineShader);
		glBindBuffer(GL_ARRAY_BUFFER, _wireFrame);

		GLuint aPosition = glGetAttribLocation( GLRender_LineShader, "aPosition" );
		glEnableVertexAttribArray(aPosition);
		glVertexAttribPointer(aPosition,  3, GL_FLOAT, GL_FALSE, 0, 0);

		glUniformMatrix4fv(glGetUniformLocation(GLRender_LineShader, "uModel"), 1, GL_TRUE, (GLfloat*)&model);
		glUniformMatrix4fv(glGetUniformLocation(GLRender_LineShader, "uView"), 1, GL_TRUE, (GLfloat*)&view);
		glUniformMatrix4fv(glGetUniformLocation(GLRender_LineShader, "uProj"), 1, GL_TRUE, (GLfloat*)&proj);
		glUniform4f(glGetUniformLocation(GLRender_LineShader, "uColor"), FrameColor.x, FrameColor.y, FrameColor.z, 1);;
		glDrawArrays(GL_LINE_STRIP, 0, 20);
	}

	// TODO remove reset to white
	FrameColor = vec3(1);
}
/*---------------------------------------------------------------------------*/
int Volume::GetRecursiveTriCount(int depth){
	int count = 0;

	if(!depth || Children[0] == NULL){
		// Assume that the vbo can always have the max
		count += 5; 
	}
	else{
		for(int i = 8; i--;){
			count += Children[i]->GetRecursiveTriCount(depth - 1);
		}
	}
	
	return count;
}
/*---------------------------------------------------------------------------*/
void Volume::CopyTris(Vertex* buffer, int depth){
	if(!depth){
		// Always do 5 tris
		int vs = sizeof(Vertex);
		memcpy(buffer + StartIndex, Verts, vs * 15);
	}
	else{
		if(Children[0]) // does this have children?
		for(int i = 8; i--;){
			Children[i]->CopyTris(buffer, depth - 1);
		}
	}
}
/*---------------------------------------------------------------------------*/
float Volume::Intersect(Ray ray){
	float t = 0;
	// TODO may want to pre calculate teh center
	vec3 i = InterPlane(ray, _center, -ray.Direction, &t);
	
	if(CONTAINS(i)){
		return t;
	}

	return 0.0f;
}
/*---------------------------------------------------------------------------*/
void Volume::getAdj(int corner, int* Adj){
	int adjPtr = 0;

	for(int i = 12; i--;){
		int* edge = EdgeList[i];
		int opposite = 0;
		for(int j = 2; j--;){
			if(edge[j] != corner)
				opposite = edge[j];
		}

		// record the adj vertex
		Adj[adjPtr++] = opposite;
	}
}
/*---------------------------------------------------------------------------*/
