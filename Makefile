FLAGS = -g -Wall -Wno-unused-function -L/usr/local/lib
SRC=./src/*.cpp ./src/Math/*.cpp ./src/UI/*.cpp ./src/Brushes/*.cpp
TESTS=tests.cpp
TOOL=main.cpp
TESTSOUT=./bin/tests
LIBS=-lGLEW -lglut -lX11 -lGL -lGLU -lm -lstdc++ -lpng -lz
INCLUDE = -I./include -I/usr/share/include -I/usr/local/libpng/include

test:
	g++ -o $(TESTSOUT) $(FLAGS) $(INCLUDE) $(SRC) $(TESTS) $(LIBS)
clean:
	rm -f ./bin/*.o $(TESTSOUT)
