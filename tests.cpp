#include "include/ConfigTables.h"
#include "include/Renderer/GLRenderer.h"
#include "include/Camera/SculptCamera.h"
#include "include/ImageLoader.h"
#include "include/Texture3D.h"
#include "include/Volume.h"
#include "include/FlatWorld.h"
#include "include/RoundWorld.h"
#include "include/DensityTexture.h"
#include "include/UI/MousePicker.h"
#include "include/Brushes/Brush.h"
#include "include/Brushes/SphereBrush.h"
#include "include/Brushes/BubbleBrush.h"
#include <cfloat>

#define DEPTH 5
#define WIDTH 1024
#define HEIGHT 600

static int window;
static int menu_id;
static int submenu_id;
static int submenuTools_id;
static int value = 0; 
float Volume::DELTACELL = 0;

// Camera Matrices
extern mat4 GLCam_View;
extern mat4 GLCam_Proj;
extern mat4 GLCam_Orientation;
extern vec4 CameraPosition;
extern vec4 CameraLook;
extern vec4 CameraDir;
extern int _lastMouseX, _lastMouseY;
extern float _yaw, _pitch;


static GLuint VoxelProgram;
Volume* root;
SculptCamera* cam;
MousePicker* picker;
Texture3D testTex;
float angle = 0.0f;
Ray ray;
Brush *bubble, *sphere, *brush;
int cutting = false;
int keys[256];
int shiftDown = false;

GLuint TexDirt, TexGrass;

void DiagRay(int x, int y){
  ray = picker->GetPickingRay(x, y);
}

void KeyDown(unsigned char key, int x, int y){
  printf(
    "%d\n", (int)key);
  keys[key] = true;

	if(glutGetModifiers() == GLUT_ACTIVE_SHIFT){
		shiftDown = true;
	}
}
void KeyUp(unsigned char key, int x, int y){
  keys[key] = false;

	if(glutGetModifiers() == GLUT_ACTIVE_SHIFT){
		shiftDown = false;
	}
}

void Keyboard(){
  /*switch ( key ) {
    //case 033:
      case 'w':
        cam->MoveForward(1.0f);
          break;
      case 's':
        cam->MoveBackward(1.0f);
          break;
      case 'a':
        cam->MoveLeft(1.0f);
          break;
      case 'd':
        cam->MoveRight(1.0f);
          break;
      case 'q':
        cam->MoveUp(1.0f);
          break;
      case 'e':
        cam->MoveDown(1.0f);
          break;
  }

  cam->Update();*/
  float delta = 0.5f;

  if(keys['w']){ cam->MoveForward(delta); }
  if(keys['s']){ cam->MoveBackward(delta); }
  if(keys['a']){ cam->MoveLeft(delta); }
  if(keys['d']){ cam->MoveRight(delta); }
  if(keys['e']){ cam->MoveUp(delta); }
  if(keys['q']){ cam->MoveDown(delta); }

  cam->Update();
}

void Highlight(Volume* v){
  float t = v->Intersect(ray);
  if(t > 0){
      std::cout << "Sln: " << t << "\n";
      v->FrameColor = vec3(1, 0, 0);
      v->BindVolume(VoxelProgram, 0, 0, 0);
      v->DrawVolume(mat4(1), *cam->GetView(), *cam->GetProj());

      if(!v->HasChildren) return;

      for(int i = 0; i < 8; i++){
        Highlight(v->Children[i]);
      }
  }
  /*else{
      v->FrameColor = vec3(1);
      v->BindVolume(VoxelProgram);
      v->DrawVolume(mat4(1), *cam->GetView(), *cam->GetProj());
  }*/
}

Volume* GetEditable(Volume* v, float* t, int depth){
  float temp = v->Intersect(ray);
  //char config = v->GetConfig();
  if(temp > 0){
      if(depth == 0){
        // this is the new closest
        *t = temp;
        return v;
      }
      else{
        //if(!v->HasChildren) return NULL;

        // few vars to track closest
        float tt = 0, nearest = FLT_MAX;
        Volume* closest = NULL;

        // for each child
        for(int i = 0; i < 8; i++){
          Volume* c = GetEditable(v->Children[i], &tt, depth - 1);

          // is this child closer?
          if(tt < nearest && c != NULL && c->GetConfig() == 0xFF){
            closest = c;
            nearest = tt;
          }
        }

        *t = nearest;
        return closest;
      }
  }
  
  // no intersection
  *t = 0;
  return NULL;
}

void Draw(){
	// todo
  Keyboard();
  float t = 0;
  Volume* v = GetEditable(root, &t, DEPTH);
  
  glEnable(GL_DEPTH_TEST);

//Highlight(root);
  root->BindVolume(VoxelProgram, TexDirt, TexGrass, TexDirt);
  root->DrawVolume(mat4(1), *cam->GetView(), *cam->GetProj());


  if(v){
    v->FrameColor = vec3(1, 0, 0);
    v->BindVolume(VoxelProgram, TexDirt, TexGrass, TexDirt);
    v->DrawVolume(mat4(1), *cam->GetView(), *cam->GetProj());  
  }
}

void ReconstructTris(Volume* v){
  vec3 c = v->GetCenter();
  Volume* r = v->GetRoot();
  float dx = (v->Max.x - c.x) * 2;
  float dy = (v->Max.y - c.y) * 2;
  float dz = (v->Max.z - c.z) * 2;

  int extent = 3;

  for(int i = -extent; i <= extent; i++)
    for(int j = -extent; j <= extent; j++)
      for(int k = -extent; k <= extent; k++){
        Volume* v = r->GetContainingVolume(c + vec3(dx * i, dy * j, dz * k));

        if(v){
          v->GenTris(v->GetConfig());
          r->UpdateVBO(v->StartIndex, v->Verts);
        }
      }
}

void MouseMove(int x, int y){
  DiagRay(x, y);

  /*printf("Pos (%f, %f, %f)\nDir (%f, %f, %f)\n",
    r.Position.x, r.Position.y, r.Position.z,
    r.Position.x + r.Direction.x, r.Position.y + r.Direction.y, r.Position.z + r.Direction.z
  );*/
  _lastMouseX = x; _lastMouseY = y;
}

void MouseHandeler(int x, int y){
  DiagRay(x, y);

if(keys['r'])
  cam->Update(x, y);

  float t = 0;
  Volume* v = GetEditable(root, &t, DEPTH);

  // on middle mouse
  if(v && cutting && !keys['r']){
    vec3 nearest = vec3(0);//v->GetNearestCorner(ray);
      if(value == 2)
      brush->Extrude(v, nearest, 0.2f);
      if(value == 3)
      brush->Carve(v, nearest, 0.2f);
    
    //ReconstructTris(v);

    //root->GenTrisRecursive();
    //root->UpdateVBO(DEPTH);
  }

  _lastMouseX = x; _lastMouseY = y;
}

void OnClick(int button, int state, int x, int y){
  DiagRay(x, y);

  if(button == 3) brush->DecreaseRadius(0.1f);
  if(button == 4) brush->IncreaseRadius(0.1f);



  if(button == 0 && !keys['r']){
    cutting = true;

    float t = 0;
    Volume* v = GetEditable(root, &t, DEPTH);

    if(v){
      vec3 nearest = vec3(0);//v->GetNearestCorner(ray);
      if(value == 2)
      brush->Extrude(v, nearest, 0.1f);
      if(value == 3)
      brush->Carve(v, nearest, 0.1f);
    }
  }
  

  cam->ResetLastMouse(x, y);
  _lastMouseX = x; _lastMouseY = y;
}

void menu(int num){
  if(num == 0){
    glutDestroyWindow(window);
    exit(0);
  }else{
    value = num;

    if(value == 4) brush = sphere;
    if(value == 5) brush = bubble;
  }
  glutPostRedisplay();
} 

void createMenu(void){
    submenu_id = glutCreateMenu(menu);
    glutAddMenuEntry("Extrude", 2);
    glutAddMenuEntry("Carve", 3);

    submenuTools_id = glutCreateMenu(menu);
    glutAddMenuEntry("Sphere", 4);
    glutAddMenuEntry("Bubble", 5);

    menu_id = glutCreateMenu(menu);
    glutAddSubMenu("Tools", submenuTools_id);
    glutAddSubMenu("Modes", submenu_id);
    glutAddMenuEntry("Clear", 1);
    glutAddMenuEntry("Quit", 0);
    glutAttachMenu(GLUT_RIGHT_BUTTON);
} 

void init(){
	createMenu();
	glClearColor(0.1f, 0.2f, 0.6f, 1.0f);

  ImageLoader::readfile("./Images/dirt.png", &TexDirt);
  ImageLoader::readfile("./Images/grass.png", &TexGrass);

  picker = new MousePicker(WIDTH, HEIGHT, cam);
	root = new Volume(NULL, vec3(-6), vec3(6));
  testTex = LoadTexure3D("./3dflat.png");
  //root->SetDensity(new SphereDensity(root, 5.0f, (int)pow(pow(8, DEPTH), 1.0/3.0)));
  root->SetDensity(new FlatDensity(root, 0.0f, (int)pow(pow(8, DEPTH), 1.0/3.0)));
	//root->SetDensity(new DensityTexture(root, &testTex));

  brush = bubble = new BubbleBrush(1.0f, 12.0f / 32.0f);
  sphere = new SphereBrush(1.0f, 12.0f / 32.0f);
  //new SphereDensity(5);//FlatDensity(-0.21f);
	/*printf("config: %u\n", root->GetConfig());
	root->GenTris(root->GetConfig());
	root->UpdateVBO(0);
	printf("Tri count: %d\n", root->TriCount);*/
	VoxelProgram = GLRenderInitShader("./Shaders/Voxel.vert", "./Shaders/Voxel.frag");

  bzero(keys, sizeof(int) * 256);

	//glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );

  glutMotionFunc(MouseHandeler);
  glutMouseFunc(OnClick);
  glutPassiveMotionFunc(MouseMove);
  glutKeyboardFunc(KeyDown);
  glutKeyboardUpFunc(KeyUp);

  int depth = DEPTH, vertexOffset = 0;
	root->SubdivideAndGenTris(depth, &vertexOffset);
	root->UpdateVBO(depth);
	root->BindVolume(VoxelProgram, 0, 0, 0);
}

int main(int argv, char* argc[]){
	CTconfig = new ConfigTables();
	CTconfig->Load("tables.txt");
	//CTconfig = Tables;

	printf("Creating render context...");

	GLRenderContext creation = {
		WIDTH,               // window width
		HEIGHT,                // window height
		"Terra Sculpt",     // window title
		argv,               // cmd line arg count
		argc,               // cmd line args
		// callback functions
		init,     // init
		Draw, 
		NULL,     // keyboard
		NULL      // cleanup
	};

	printf("created!\n");

  cam = new SculptCamera(vec3(0, 12, 12), vec3(0), creation.Width / (float)creation.Height);
  cam->IsFlying = true;

	GLRender_Init(creation);

	return 1;
}
