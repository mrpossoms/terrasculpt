#version 130

attribute vec3 aPosition;
attribute vec3 aNormal;
attribute vec2 aTexCoord;
attribute int aSrcTex0;
attribute int aSrcTex1;
attribute float aTexWeight;

uniform mat4 uModel;
uniform mat4 uView;
uniform mat4 uProj;

varying vec3 vNormal;
varying vec3 vWorldPosition;

void main(){
        vNormal = aNormal;
        vec4 world = uModel * vec4(aPosition, 1.0);
        gl_Position = uProj * uView * world;

        vWorldPosition = world.xyz;
}
