uniform vec4 uColor;

varying float vDepth;

void main(void){
	gl_FragColor = uColor * vDepth;
}
