attribute vec3 aPosition;

uniform mat4 uView;
uniform mat4 uProj;
uniform mat4 uModel;
uniform vec4 uColor;

varying float vDepth;

void main(void){
	gl_Position = uProj * uView * uModel * vec4(aPosition, 1.0);
	vDepth = gl_Position.z / gl_Position.w;
}
