#version 130

uniform sampler2D uXtexture;
uniform sampler2D uYtexture;
uniform sampler2D uZtexture;

varying vec3 vNormal;
varying vec3 vWorldPosition;

void main(){
		vec4 xcol = texture2D(uXtexture, vec2(vWorldPosition.z, vWorldPosition.y));
		vec4 ycol = texture2D(uYtexture, vec2(vWorldPosition.x, vWorldPosition.z));
		vec4 zcol = texture2D(uZtexture, vec2(vWorldPosition.x, vWorldPosition.y));

		// calculate weights
		vec3 w = vec3(abs(vNormal));
		//w = max((w - 0.2) * 7, 0);

		w *= w;

		float sum = w.x + w.y + w.z;
		w /= sum;

		vec4 color = (w.x * xcol) + (w.y * ycol) + (w.z * zcol);

		float dl = -dot(vNormal, normalize(vec3(-1, -1, 0)));
        gl_FragColor = clamp(vec4(color.xyz * dl, 1.0) + vec4(0.1, 0.1, 0.1, 0.0), 0.0, 1.0);
}
