#ifndef SCULPT_CAMERA
#define SCULPT_CAMERA

#include "../Math/mat.h"

class SculptCamera{
public:
	SculptCamera(vec3 position, vec3 look, float aspect){
		IsFlying = false;

		_pos = position;
		_look = look;
		_up = vec3(0, 1, 0);

		_proj = Perspective(
			90.0f,  // fov
			aspect, // aspect
			0.1f,   // near
			1000.0f // far
		);

		//std::cout << "Forward: " << forward << "\n" << "Left: " << left << "\n" << "Up: " << up << "\n";

		_view = LookAt(
			vec4(_pos.x, _pos.y, _pos.z, 0),
			vec4(_look.x, _look.y, _look.z, 0),
			vec4(_up, 0)
		);

		RecalculateMats();
	}

	void ResetLastMouse(int x, int y){
		_lastMouseX = x;
		_lastMouseY = y;
	}

	void Update(){
		RecalculateMats();		
	}

	void Update(int x, int y){
		float dx = (x - _lastMouseX) / 10.0f;
		float dy = (_lastMouseY - y) / 10.0f;
		_lastMouseX = x; _lastMouseY = y;

		if(IsFlying){
			_yaw += dx / 10.0f;
			_pitch += dy / 10.0f;

			// avoid gimble lock
			if(_pitch > M_PI * 0.49f) _pitch = M_PI * 0.49f;
			if(_pitch < -M_PI * 0.49f) _pitch = -M_PI * 0.49f;
		}
		else{
			_orientation = RotateY(dx) * RotateX(-dy) * _orientation;
		}

		RecalculateMats();
	}

	void RecalculateMats(){
		if(IsFlying){
			_orientation = RotateY((_yaw/M_PI) * 180.0f) * RotateX((_pitch/M_PI) * 180.0f);

			vec4 camDir = _orientation * vec4(0, 0, 1, 1);
			vec4 camUp = _orientation * vec4(0, 1, 0, 1);

			// calculate camera direction vectors
			_forward = normalize(vec3(camDir.x, camDir.y, camDir.z));
			_up = normalize(vec3(camUp.x, camUp.y, camUp.z));
			_left = normalize(cross(_forward, _up));

			_look = _forward + _pos;
			_view = LookAt(
				vec4(_pos.x, _pos.y, _pos.z, 0),
				vec4(_look.x, _look.y, _look.z, 0),
				vec4(_up, 0)
			);
		}
		else{

		}
	}

	void MoveForward(float delta){
		_pos += _forward * delta;
	}

	void MoveBackward(float delta){
		_pos -= _forward * delta;	
	}

	void MoveUp(float delta){
		_pos += _up * delta;
	}

	void MoveDown(float delta){
		_pos -= _up * delta;	
	}
	void MoveLeft(float delta){
		_pos -= _left * delta;
	}

	void MoveRight(float delta){
		_pos += _left * delta;
	}

	mat4* GetView(){ return &_view; }
	mat4* GetProj(){ return &_proj; }
	mat4* GetOrin(){ return &_orientation; }

	vec3 GetPosition(){ return _pos; }
	vec3 GetLook(){ return _look; }
	vec3 GetLeft(){ return _left; }
	vec3 GetUp(){ return _up; }
	vec3 GetForward(){ return _forward; }

	bool IsFlying;
private:
	mat4 _view, _proj, _orientation;
	vec3 _pos, _look, _up, _left, _forward;
	float _yaw, _pitch;
	int _lastMouseX, _lastMouseY;
};

#endif
