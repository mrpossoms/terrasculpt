#ifndef DENSITY_FUNCTION
#define DENSITY_FUNCTION
#include "Math/vec.h"
#include <iostream>

using namespace std;

class DensityFunc{
public:
	DensityFunc(){}
	virtual float Sample(vec3 worldSpace) = 0;
	virtual float TrilinearSample(vec3 worldSpace) = 0;
	virtual void Update(vec3 worldSpace, float value) = 0;
	virtual void Increment(vec3 worldSpace, float delta) = 0;
	virtual int Resolution() = 0;
	virtual unsigned char*** Get3DTexture() = 0;
};

#endif
