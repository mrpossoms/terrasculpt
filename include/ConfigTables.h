#ifndef CONFIG_TABLES 
#define CONFIG_TABLES
#define CONFIG_TABLES_DEBUG
#include <stdio.h>

class ConfigTables{
public:
	ConfigTables(){}
	int Load(const char* path);

	int TriCount[256];
	int TriLists[256][15];
};
#endif
