#pragma once
#include <png.h>
#include "Renderer/GLRenderer.h"

class ImageLoader{
	public:
	/*
	  The following function (loadPngImage()) was copied from:
	  http://blog.nobel-joergensen.com/2010/11/07/loading-a-png-as-texture-in-opengl-using-libpng/

	  Dr. Kuhl has different code which loads images using ImageMagick, but
	  ImageMagick is not properly installed in the lab machines. Due to a
	  last-minute scrambling, we are using this code found online. This code
	  basically just uses libpng to load PNG files.
	*/
	static bool loadPngImage(const char *name, int &outWidth, int &outHeight, bool &outHasAlpha, unsigned char **outData) {
	    png_structp png_ptr;
	    png_infop info_ptr;
	    unsigned int sig_read = 0;
	    //    int color_type, interlace_type;
	    FILE *fp;

	    if ((fp = fopen(name, "rb")) == NULL)
		return false;

	    /* Create and initialize the png_struct
	     * with the desired error handler
	     * functions.  If you want to use the
	     * default stderr and longjump method,
	     * you can supply NULL for the last
	     * three parameters.  We also supply the
	     * the compiler header file version, so
	     * that we know if the application
	     * was compiled with a compatible version
	     * of the library.  REQUIRED
	     */
	    png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING,
		    NULL, NULL, NULL);

	    if (png_ptr == NULL) {
		fclose(fp);
		return false;
	    }

	    /* Allocate/initialize the memory
	     * for image information.  REQUIRED. */
	    info_ptr = png_create_info_struct(png_ptr);
	    if (info_ptr == NULL) {
		fclose(fp);
		png_destroy_read_struct(&png_ptr, NULL, NULL);
		return false;
	    }

	    /* Set error handling if you are
	     * using the setjmp/longjmp method
	     * (this is the normal method of
	     * doing things with libpng).
	     * REQUIRED unless you  set up
	     * your own error handlers in
	     * the png_create_read_struct()
	     * earlier.
	     */
	    if (setjmp(png_jmpbuf(png_ptr))) {
		/* Free all of the memory associated
		 * with the png_ptr and info_ptr */
		png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
		fclose(fp);
		/* If we get here, we had a
		 * problem reading the file */
		return false;
	    }

	    /* Set up the output control if
	     * you are using standard C streams */
	    png_init_io(png_ptr, fp);

	    /* If we have already
	     * read some of the signature */
	    png_set_sig_bytes(png_ptr, sig_read);

	    /*
	     * If you have enough memory to read
	     * in the entire image at once, and
	     * you need to specify only
	     * transforms that can be controlled
	     * with one of the PNG_TRANSFORM_*
	     * bits (this presently excludes
	     * dithering, filling, setting
	     * background, and doing gamma
	     * adjustment), then you can read the
	     * entire image (including pixels)
	     * into the info structure with this
	     * call
	     *
	     * PNG_TRANSFORM_STRIP_16 |
	     * PNG_TRANSFORM_PACKING  forces 8 bit
	     * PNG_TRANSFORM_EXPAND forces to
	     *  expand a palette into RGB
	     */
	    png_read_png(png_ptr, info_ptr, PNG_TRANSFORM_STRIP_16 | PNG_TRANSFORM_PACKING | PNG_TRANSFORM_EXPAND, NULL);

	    outWidth = png_get_image_width(png_ptr, info_ptr);
	    outHeight = png_get_image_height(png_ptr, info_ptr);
	    png_byte color_type = png_get_color_type(png_ptr, info_ptr);

	    switch (color_type) {
		case PNG_COLOR_TYPE_RGBA:
		    outHasAlpha = true;
		    break;
		case PNG_COLOR_TYPE_RGB:
		    outHasAlpha = false;
		    break;
		default:
		    std::cout << "Color type " << color_type << " not supported" << std::endl;
		    png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
		    fclose(fp);
		    return false;
	    }
	    unsigned int row_bytes = png_get_rowbytes(png_ptr, info_ptr);
	    *outData = (unsigned char*) malloc(row_bytes * outHeight);

	    png_bytepp row_pointers = png_get_rows(png_ptr, info_ptr);

	    for (int i = 0; i < outHeight; i++) {
		// note that png is ordered top to
		// bottom, but OpenGL expect it bottom to top
		// so the order or swapped
		memcpy(*outData+(row_bytes * (outHeight-1-i)), row_pointers[i], row_bytes);
	    }

	    /* Clean up after the read,
	     * and free any memory allocated */
	    png_destroy_read_struct(&png_ptr, &info_ptr, NULL);

	    /* Close the file */
	    fclose(fp);

	    /* That's it */
	    return true;
	}




	/* Readfile uses imageio to read in an image, and binds it to an OpenGL
	 * texture name.  Requires OpenGL 2.0 or better.
	 *
	 * filename: name of file to load
	 *
	 * texName: A pointer to where the OpenGL texture name should be stored.
	 * (Remember that the "texture name" is really just some unsigned int).
	 *
	 * returns: aspect ratio of the image in the file.
	 */
	static float readfile(const char *filename, GLuint* texName)
	{

		if(!GLEW_VERSION_2_0)
		{
			printf("ERROR: readline() requires OpenGL 2.0.\n");
			printf("Your video card / driver doesn't seem to support OpenGL 2.0 or better.\n");
			exit(1);
		}

		int width  = 0;
		int height = 0;
		unsigned char* data = NULL;
		bool hasAlpha = 0;
		if(loadPngImage(filename, width, height, hasAlpha, &data) == false)
		{
			fprintf(stderr, "\n%s: Unable to read image.\n", filename);
			exit(1);
	//		return -1;
		}

		if(hasAlpha)
			printf("%s: Has alpha component.\n", filename);

		/* "image" is a 1D array of characters (unsigned bytes) with four
		 * bytes for each pixel (red, green, blue, alpha). The data in "image"
		 * is in row major order. The first 4 bytes are the color information
		 * for the lowest left pixel in the texture. */
		float original_aspectRatio = (float)(width)/(height);
		// if(verbose)
		// 	printf("%s: Finished reading, dimensions are %dx%d\n",
		// 	       filename, width, height);

		glGenTextures(1, texName);
		glBindTexture(GL_TEXTURE_2D, *texName);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE); // multiply color and texture

		/* If anisotropic filtering is available, turn it on.  This does not
		 * override the MIN_FILTER. The MIN_FILTER setting may affect how the
		 * videocard decides to do anisotropic filtering, however.  For more info:
		 * http://www.opengl.org/registry/specs/EXT/texture_filter_anisotropic.txt
		 */
		if(glewIsSupported("GL_EXT_texture_filter_anisotropic"))
		{
			float maxAniso;
			glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &maxAniso);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, maxAniso);
/*			if(verbose)
				printf("%s: Anisotropic filtering: Available, set to maximum value (%0.1f)\n",
				       filename, maxAniso);*/
		}
		else
		{
/*			if(verbose)
				printf("%s: Anisotropic filtering: Not available\n", filename);*/
		}


		/* Try to see if OpenGL will accept this texture.  If the dimensions of
		 * the file are too big, OpenGL might not load it. NOTE: The parameters
		 * here should match the parameters of the actual (non-proxy) calls to
		 * glTexImage2D() below. */
		glTexImage2D(GL_PROXY_TEXTURE_2D, 0, hasAlpha ? GL_RGBA : GL_RGB, width, height,
			     0, hasAlpha ? GL_RGBA : GL_RGB, GL_UNSIGNED_BYTE, NULL);
		int tmp;
		glGetTexLevelParameteriv(GL_PROXY_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &tmp);
		if(tmp == 0)
		{
			fprintf(stderr, "%s: File is too large.  I can't load it!\n", filename);
			//free(image);
			return -1;
		}

		/* The recommended way to produce mipmaps depends on your OpenGL
		 * version. */
		if (glGenerateMipmap != NULL)
		{
			/* In OpenGL 3.0 or newer, it is recommended that you use
			 * glGenerateMipmaps().  Older versions of OpenGL that provided the
			 * same capability as an extension, called it
			 * glGenerateMipmapsEXT(). */
/*			if(verbose)
				printf("%s: Mipmaps: Generating with glGenerateMipmap().\n", filename);*/
			glTexImage2D(GL_TEXTURE_2D, 0, hasAlpha ? GL_RGBA : GL_RGB, width, height,
				     0, hasAlpha ? GL_RGBA : GL_RGB, GL_UNSIGNED_BYTE, data);
			glGenerateMipmap(GL_TEXTURE_2D);
		}
		else // if(glewIsSupported("GL_SGIS_generate_mipmap"))
		{
			/* Should be used for 1.4 <= OpenGL version < 3.   */
/*			if(verbose)
				printf("%s: Mipmaps: Generating with GL_GENERATE_MIPMAP.\n", filename);
			glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
			glTexImage2D(GL_TEXTURE_2D, 0, hasAlpha ? GL_RGBA : GL_RGB, *width, *height,
				     0, hasAlpha ? GL_RGBA : GL_RGB, GL_UNSIGNED_BYTE, image);*/
		}


		/* render textures perspectively correct---instead of interpolating
		   textures in screen-space. */
		// glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

		//free(image);
		free(data);
		return original_aspectRatio;
	}
};
