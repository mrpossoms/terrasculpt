#ifndef FLAT_WORLD
#define FLAT_WORLD

#include "DensityFunc.h"
#include "DensityTexture.h"
#include "Texture3D.h"

class FlatDensity : public DensityTexture{
public:
        FlatDensity(Volume* root, float height, int res)
        	:DensityTexture(root, CreateTexture3D(res)){
                _height = height;

//std::cout << "Inserting values!!\n";
        for(int x = res+1; x--;)
            for(int y = res+1; y--;)
                for(int z = res+1; z--;){
                    vec3 p(x/(float)res, y/(float)res, z/(float)res);
                    vec3 ws = (root->Max * p) + (root->Min * (vec3(1) - p));
                    Update(ws, -ws.y + _height);
                }
//std::cout << "Inserting done\n"; 
        }

private:
        float _height;
};

#endif

