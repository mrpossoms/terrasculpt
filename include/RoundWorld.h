#ifndef ROUND_WORLD
#define ROUND_WORLD

#include "DensityFunc.h"
#include "Texture3D.h"
#include "DensityTexture.h"

class SphereDensity : public DensityTexture{
public:
    SphereDensity(Volume* root, float radius, int res)
        :DensityTexture(root, CreateTexture3D(res)){
        _radius = radius;
        std::cout << "Created OK!\n";

        for(int x = res+1; x--;)
            for(int y = res+1; y--;)
                for(int z = res+1; z--;){
                    vec3 p(x/(float)(res), y/(float)res, z/(float)res);
                    vec3 ws = (root->Max * p) + (root->Min * (vec3(1) - p));
                    Update(ws, _radius - length(ws));
                }
    }

    /*float Sample(vec3 worldSpace){
        float d = length(worldSpace) - _radius;
        d = d > 1 ? 1 : (d < -1 ? -1 : d);
        return d;
        //return TrilinearSample(worldSpace);
    }*/
private:
        float _radius;
};

#endif
