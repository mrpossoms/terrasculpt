#ifndef SCULPT_BRUSH_SPHERE
#define SCULPT_BRUSH_SPHERE

#include "Brush.h"

class SphereBrush : public Brush{
public:
	SphereBrush(float radius, float step);

	void Extrude(Volume* volume, vec3 Coord, float delta);
	void Carve(Volume* volume, vec3 Coord, float delta);
	void Smooth(Volume* volume, vec3 Coord, float delta);
	void Flatten(Volume* volume, vec3 Coord, float delta);

	void IncreaseRadius(float delta);
	void DecreaseRadius(float delta);

private:
	float _step; // world distance between voxels in the texture
	int _iterations;
	vec3 _interval;

	vec3 _min, _max;
};

#endif