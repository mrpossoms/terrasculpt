#ifndef SCULPT_BRUSH_BUBBLE
#define SCULPT_BRUSH_BUBBLE

#include "Brush.h"
//#include "../DensityTexture.h"

class BubbleBrush : public Brush{
public:
	BubbleBrush(float radius, float step);

	void Extrude(Volume* volume, vec3 Coord, float delta);
	void Carve(Volume* volume, vec3 Coord, float delta);
	void Smooth(Volume* volume, vec3 Coord, float delta);
	void Flatten(Volume* volume, vec3 Coord, float delta);

	void IncreaseRadius(float delta);
	void DecreaseRadius(float delta);

private:
	float _step; // world distance between voxels in the texture
	int _iterations;
	vec3 _interval;

	vec3 _min, _max;

	int isAdjacent(Volume* v, int x, int y, int z);
};

#endif