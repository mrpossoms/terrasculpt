#ifndef SCULPT_BRUSH
#define SCULPT_BRUSH

#include "../Volume.h"
#include "../DensityFunc.h"
#include "../Math/vec.h"

#define RECONSTRUCT(v, extent){\
  int res = v->Density->Resolution()+1;\
  Volume* r = v->GetRoot();\
  r->BindVBO();\
  for(int i = -extent; i <= extent; i++){\
    for(int j = -extent; j <= extent; j++){\
      for(int k = -extent; k <= extent; k++){\
        Index3D ind = v->VolIndex;\
        int x = ind.x+i, y = ind.y+j, z = ind.z+k;\
        if(x>=0&&x<res&&y>=0&&y<res&&z>=0&&z<res){\
          Volume* cv = r->Leaves[ind.x+i][ind.y+j][ind.z+k];\
          if(cv){\
            cv->GenTris(cv->GetConfig());\
            r->UpdateVBO(cv->StartIndex, cv->Verts);\
          }\
        }\
      }\
    }\
  }\
}\

class Brush{
public:
	Brush(float radius){ _radius = radius; }

	void SetRadius(float r){ _radius = r; }

	virtual void Extrude(Volume* volume, vec3 Coord, float delta) = 0;
	virtual void Carve(Volume* volume, vec3 Coord, float delta) = 0;
	virtual void Smooth(Volume* volume, vec3 Coord, float delta) = 0;
	virtual void Flatten(Volume* volume, vec3 Coord, float delta) = 0;

  virtual void IncreaseRadius(float delta) = 0;
  virtual void DecreaseRadius(float delta) = 0;

protected:
	float _radius;
};

#endif
