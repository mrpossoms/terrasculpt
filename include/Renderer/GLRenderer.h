#pragma once
#ifndef GL_RENDERER
#define GL_RENDERER

#include <stdio.h>
#include <iostream>

#include <GL/glew.h>
#include <GL/freeglut.h>
#include <GL/freeglut_ext.h>
#include <GL/gl.h>

#include "../Math/mat.h"
#include "ShaderCompiler.h"

/*--- Structs----------------------------------------------------------------*/
typedef struct{
	int Width;
	int Height;
	const char* Title;
	int argv;
	char** argc;
	void (*InitCallback)();
	void (*DrawCallback)();
	void (*KeyboardCallback)(unsigned char key, int x, int y);
	void (*ExitCallback)();
} GLRenderContext;

/*--- Globals ---------------------------------------------------------------*/
// user rendering callback
void (*DrawCallback)();

// Camera Matrices
static mat4 GLCam_View;
static mat4 GLCam_Proj;
static mat4 GLCam_Orientation;
static vec4 CameraPosition;
static vec4 CameraLook;
static vec4 CameraDir;
static int _lastMouseX, _lastMouseY;
static float _yaw, _pitch;

// Line and quad vars
GLuint GLRender_LineShader;
GLuint GLRender_LineBuffer;
GLuint GLRender_Quad;

/*--- Function definitions --------------------------------------------------*/
mat4 GetCamView(){ return GLCam_View; }

mat4 GetCamProj(){ return GLCam_Proj; }

void GLRenderDraw(){
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	GLCam_View = LookAt(
		vec4(CameraPosition[0], CameraPosition[1], CameraPosition[2], 0),
		vec4(CameraLook[0], CameraLook[1], CameraLook[2], 0),
		vec4(0, 1, 0, 0)
	);

	if(DrawCallback) DrawCallback();

	glFlush();
	glutPostRedisplay();
	glutSwapBuffers();
}

void GLRender_DrawLine(mat4 model, mat4 view, mat4 proj){
			// make sure we are using the right buffer
			glBindBuffer(GL_ARRAY_BUFFER, GLRender_LineShader); 

			// Initialize the vertex position attribute from the vertex shader
			GLuint vPosition = glGetAttribLocation( GLRender_LineShader, "aPosition" );
			glEnableVertexAttribArray(vPosition);
			glVertexAttribPointer(vPosition, 3, GL_FLOAT, GL_FALSE, 0, 0);

			GLuint uModel = glGetUniformLocation(GLRender_LineShader, "uModel");
			GLuint uView  = glGetUniformLocation(GLRender_LineShader, "uView");
			GLuint uProj  = glGetUniformLocation(GLRender_LineShader, "uProj");

			glUniformMatrix4fv(uView, 1, GL_TRUE, (GLfloat*)&uModel);
			glUniformMatrix4fv(uView, 1, GL_TRUE, (GLfloat*)&view);
			glUniformMatrix4fv(uProj, 1, GL_TRUE, (GLfloat*)&proj);

			glDrawArrays(GL_LINES, 0, 2);
}

void GLRender_CustomLine(GLfloat verts[6]){

	glBufferData(GL_ARRAY_BUFFER,
		     6 * sizeof(GLfloat),
	             verts, GL_STATIC_DRAW );
}

void mouse(int x, int y){
	float dx = (x - _lastMouseX) / 10.0f;
	float dy = (y - _lastMouseY) / 10.0f;

	_yaw += dx;
	_pitch += dy;

	// avoid gimble lock
	if(_pitch > M_PI * 0.49f) _pitch = M_PI * 0.49f;
	if(_pitch < -M_PI * 0.49f) _pitch = -M_PI * 0.49f;

	GLCam_Orientation = RotateY(dx) * RotateX(dy) * GLCam_Orientation;
	CameraDir = GLCam_Orientation * vec4(0, 0, 1, 1);
	CameraLook = CameraDir + CameraPosition;

	_lastMouseX = x; _lastMouseY = y;
}

void mouseMove(int button, int state, int x, int y){
    if(state == GLUT_DOWN){
	//cur_button = button;

		_lastMouseX = x;
		_lastMouseY = y;

    }
}

int GLRender_Init(GLRenderContext ctx){
	printf("glut initializing...");

	// initialize glut, create our window
	glutInit(&ctx.argv, ctx.argc);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);
	glutInitWindowSize(ctx.Width, ctx.Height);
	glutCreateWindow(ctx.Title);

	printf("initialized!\n");

	// initialize glew:
	int glew_err = glewInit();
	if(glew_err != GLEW_OK)
		fprintf(stderr, "GLEW Error: %s\n", glewGetErrorString(glew_err));

	DrawCallback = ctx.DrawCallback;

	// register callback functions
	glutDisplayFunc(GLRenderDraw);
	glutKeyboardFunc(ctx.KeyboardCallback);
	glutCloseFunc(ctx.ExitCallback);

	GLRender_LineShader = GLRenderInitShader("./Shaders/line.vert", "./Shaders/line.frag");

	glGenBuffers(1, &GLRender_LineBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, GLRender_LineBuffer);
	float lineVerts[6] = {
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f
	};
	GLRender_CustomLine(lineVerts);

	glClearDepth(1.0f);

	GLCam_Proj = Perspective(
		90.0f,                         // fov
		ctx.Width / (float)ctx.Height, // aspect
		0.1f,                          // near
		1000.0f                        // far
	);
	glEnable(GL_DEPTH_TEST);

	// initialize mousemove stuff.
	glutMotionFunc(mouse);
	glutMouseFunc(mouseMove);
	CameraPosition = vec4(0, 6.0f, 0, 1);

	if(ctx.InitCallback) ctx.InitCallback();

	glutMainLoop();	// hand over control to glut/opengl

	return 1;
}
#endif
