#ifndef TEXTURE_WORLD
#define TEXTURE_WORLD

#include "DensityFunc.h"
#include "Texture3D.h"
#include "Volume.h"

class DensityTexture : public DensityFunc{
public:
        DensityTexture(Volume* volume, Texture3D* texture){
                _voxel = volume;
                _texture = texture;
        }

        float Sample(vec3 worldSpace){
                return SampleTexture3D(_voxel, _texture, worldSpace);
        }

        float TrilinearSample(vec3 worldSpace){
                return TriliniarSampleTexture3D(_voxel, _texture, worldSpace);
        }

        void Update(vec3 worldSpace, float value){
                UpdateTexture3D(_voxel, _texture, worldSpace, value);
        }

        void Increment(vec3 worldSpace, float delta){
                IncrementTexture3D(_voxel, _texture, worldSpace, delta);
        }
        int Resolution(){
                return _texture->Width;
        }

        unsigned char*** Get3DTexture(){
                return _texture->Voxel;
        }
protected:
        Volume* _voxel;
        Texture3D* _texture;
};

#endif

