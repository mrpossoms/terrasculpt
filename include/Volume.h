#pragma once
#ifndef VOLUME
#define VOLUME

#include <GL/glew.h>
#include <GL/freeglut.h>
#include <GL/freeglut_ext.h>

#include <GL/gl.h>
#include <GL/glu.h>

#include "Math/vec.h"
#include "Math/mat.h"
#include "Math/Intersection.h"
#include "Renderer/GLRenderer.h"
#include "ConfigTables.h"
#include "DensityFunc.h"

#define CONTAINS(v) v.x <= Max.x && v.x >= Min.x &&\
	   v.y <= Max.y && v.y >= Min.y &&\
	   v.z <= Max.z && v.z >= Min.z\

#define RANDOM() ((random() % 1000) / 1000.0f)\

/*--- Structs ---------------------------------------------------------------*/
typedef struct{
	vec3  Position;
	vec3  Normal;
	vec2  TexCoord;
	int   SrcTextures[2];
	float TextWeight;
}Vertex;

typedef struct{
	int x;
	int y;
	int z;
} Index3D;

/*--- Globals ---------------------------------------------------------------*/
extern ConfigTables* CTconfig;
extern int EdgeList[12][2];

extern mat4 GLCam_View;
extern mat4 GLCam_Proj;
extern GLuint GLRender_LineShader;
/*--- Classes ---------------------------------------------------------------*/
class Volume{
public:
	Volume(Volume* parent, vec3 min, vec3 max);
	Volume** Subdivide(vec3 center);
	Volume** Subdivide();

	void SubdivideAndGenTris(int depth, int *VertexIndex);

	unsigned char GetConfig();
	vec3 SolveVert(int ei);
	void GenTris(unsigned char config);
	void GenTrisRecursive();
	vec3 GetCorner(int index);
	vec3 GetNearestCorner(Ray ray);
	vec3 GetCenter();

	void BindVBO();
	void UpdateVBO(int depth);
	void UpdateVBO(int offset, Vertex* buffer);

	Volume* GetContainingVolume(vec3 pos);
	Volume* GetRoot(){
		Volume* v = this;
		while(v->Parent != NULL) v = v->Parent;
		return v;
	}

	void BindVolume(GLuint program, GLuint xTexture, GLuint yTexture, GLuint zTexture);
	void DrawVolume(mat4 model, mat4 view, mat4 proj);

	int GetRecursiveTriCount(int depth);
	void CopyTris(Vertex* buffer, int depth); 

	float Intersect(Ray ray);
	void SetDensity(DensityFunc* func){
		DELTACELL = (Max.x - Min.x) / func->Resolution();
		Density = func;
	}
/*--- Members ---------------------------------------------------------------*/
	vec3 Min, Max;
	Volume* Parent;
	Volume* Children[8];

	int HasChildren;
	int TriCount;
	Vertex Verts[15];

	int StartIndex;

	DensityFunc* Density;
	vec3 FrameColor;

	Volume**** Leaves;
	Index3D VolIndex;
private:
	static float DELTACELL;
	void getAdj(int corner, int* Adj);

	GLuint _vertexBuffer, _wireFrame;
	int    _vboTriCount;
	GLuint _uModel, _uView, _uProj, _uColor;
	vec3   _corners[8];
	vec3   _center;

	unsigned char _config;
	int _configStale;
};

#endif

