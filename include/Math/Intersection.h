#pragma once
#ifndef INTERSECTION_MATH
#define INTERSECTION_MATH

#include <GL/gl.h>
#include "vec.h"

typedef struct{
	vec3 Position;
	vec3 Direction;
} Ray;

// returns the worlspace coordinate of the given ray's intersection
// point on the specified mathematical plane. If the intersection
// cannot be calculated (ie ray dir and plane tan are parallel) then
// the start position of the ray is returned.
vec3 InterPlane(Ray ray, vec3 planePos, vec3 normal, float *t);

#endif
