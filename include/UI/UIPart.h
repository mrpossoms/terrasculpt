#ifndef UIPART
#define UIPART

#include <vector.h>
#include "../Renderer/GLRenderer.h"
#include "../Math/vec.h"
#include "../Math/mat.h"

class UIPart{
public:
	UIPart* Parent;
	vector<UIPart*> Children;

	// where x and y lie in the cannonical view volume
	void (MouseOver*)(float x, float y);
	void (MouseClick*)(float x, float y, int button);
	void (Update*)(float dt); 
	void (Draw*)(GLuint shader);

	UIPart(vec3 min, vec3 max){
		_min = min; _max = max;
		Parent = NULL;
	}

	void AddPart(UIPart* child){
		child->Parent = this;
		Children.push_back(child);
	}

	void SetTransform(mat4 model){
		_model = model;
		_isTransStale = true;
	}

	int ContainsPoint(float x, float y){
		if(_isTransStale){
			_transMin = GLCam_Proj * _model * vec4(_min);
			_transMax = GLCam_Proj * _model * vec4(_max);
			_isTransStale = false;
		}

		// does the cursor lie in the projected quad?
		if(x >= _transMin.x && x <= _transMax.x &&
		   y >= _transMin.y && y <= _transMax.y){
			return true;
		}
	
		return false;
	} 
private:
	vec3 _min, _max;

	int _isTransStale;
	vec4 _transMin, _transMax;
	mat4 _model;
};

#endif
