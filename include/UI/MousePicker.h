#ifndef UI_MOUSE_PICKER
#define UI_MOUSE_PICKER

#include <GL/gl.h>
#include <GL/glu.h>
#include "../Math/vec.h"
#include "../Math/mat.h"
#include "../Math/Intersection.h"
#include "../Camera/SculptCamera.h"

class MousePicker{
public:
	MousePicker(int screenWidth, int screenHeight, SculptCamera* camera);
	Ray GetPickingRay(int x, int y);
	vec3 UnProject(int x, int y, mat4* view, mat4* proj);
private:
	SculptCamera* _camera;
	int _width, _height;
	int _halfWidth, _halfHeight;
};

#endif