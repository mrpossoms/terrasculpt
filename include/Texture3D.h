#ifndef TEXTURE3D
#define TEXTURE3D

#include "ImageLoader.h"
#include "Math/vec.h"
#include "Volume.h"

typedef struct{
	int Width;
	int Height;
	int Depth;
	unsigned char*** Voxel;
}Texture3D;

float SampleTexture3D(Volume* voxel, Texture3D* tex, vec3 coord){
	// calculate a coordinate between (0, 0, 0) and (1, 1, 1)

	//std::cout << coord << " ---> ";

	vec3 diff = (voxel->Max - voxel->Min);
	coord = (coord - voxel->Min) / diff;

	//float d = 1.0f / tex->Height;
	float du = 1.0f;// - d;

	coord.x = coord.x >= 1.0f ? du : coord.x;
	coord.y = coord.y >= 1.0f ? du : coord.y;
	coord.z = coord.z >= 1.0f ? du : coord.z;

	coord.x = coord.x < 0.0f ? 0 : coord.x;
	coord.y = coord.y < 0.0f ? 0 : coord.y;
	coord.z = coord.z < 0.0f ? 0 : coord.z;

	int w = tex->Width - 1;

	//std::cout << coord << " --> <" << ((int)(w * coord.x)) << ", " << ((int)(w * coord.y)) << ", " << ((int)(w * coord.z)) << "> == ";

	float value = (float)tex->Voxel[(int)(w * coord.x)][(int)(w * coord.y)][(int)(w * coord.z)];

	//std::cout << value << "\n";
	value = (value - 128.0f) / 128.0f;

	//if(value > -1.0f && value < 1.0f)

	return value;
}

float TriliniarSampleTexture3D(Volume* voxel, Texture3D* tex, vec3 coord){
	// calculate a coordinate between (0, 0, 0) and (1, 1, 1)
	vec3 diff = (voxel->Max - voxel->Min);
	coord = (coord - voxel->Min) / diff;

	float d = 1.0f / tex->Height;
	float du = 1.0f - d;
	coord.x = coord.x >= 1.0f ? du : coord.x;
	coord.y = coord.y >= 1.0f ? du : coord.y;
	coord.z = coord.z >= 1.0f ? du : coord.z;

	coord.x = coord.x < 0.0f ? 0 : coord.x;
	coord.y = coord.y < 0.0f ? 0 : coord.y;
	coord.z = coord.z < 0.0f ? 0 : coord.z;

	//std::cout << "Doing lookup at" << coord << " ";

	int w = tex->Width;
	int x = (int)(w * coord.x), y = (int)(w * coord.y), z = (int)(w * coord.z);
	int samples = 0;

	float densities[27] = {0};
	float weights[27] = {0};
	float len = 0.0f;

	for(int i = -1; i <= 1; i++)
		for(int j = -1; j <= 1; j++)
			for(int k = -1; k <= 1; k++){
				int sx = x + i, sy = y + j, sz = z + k;
				if(sx >= 0 && sx < w && sy >= 0 && sy < w && sz >= 0 && sz < w){
					int ind = ((i + 1) * 9) + ((j + 1) * 3) + k + 1;
					densities[ind] = (tex->Voxel[sx][sy][sz] - 128.0f) / 128.0f;
					weights[ind] = length(coord - vec3(sx/(float)w,sy/(float)w,sz/(float)w));
					len += weights[ind] * weights[ind]; // squared distance
					++samples;
				}
			}

	float mag = sqrt(len);
	float value = 0;

	for(int i = 27; i--;){
		value += densities[i] * (weights[i] / mag);
	}

	//std::cout << "Sum: " << sum << " Samples: " << samples << " ";
	//float value = (tex->Voxel[x][y][z] + (sum / samples)) / 2.0f;
	//std::cout << value << " --> ";
	//value = (value - 128.0f) / 128.0f;
	//std::cout << "Found: " << value << "\n";
	return value;
}

void UpdateTexture3D(Volume* voxel, Texture3D* tex, vec3 coord, float value){
	//std::cout << "||" << coord << " ---> ";

	// calculate a coordinate between (0, 0, 0) and (1, 1, 1)
	vec3 diff = (voxel->Max - voxel->Min);
	coord = (coord - voxel->Min) / diff;

	//float d = 1.0f / tex->Height;
	float du = 1.0f;// - d;
	coord.x = coord.x >= 1.0f ? du : coord.x;
	coord.y = coord.y >= 1.0f ? du : coord.y;
	coord.z = coord.z >= 1.0f ? du : coord.z;

	coord.x = coord.x < 0.0f ? 0 : coord.x;
	coord.y = coord.y < 0.0f ? 0 : coord.y;
	coord.z = coord.z < 0.0f ? 0 : coord.z;

	//std::cout << "Setting at" << coord << " ";
	// keep the sample value within -1, 1

	float d = 1.0f / 256.0f;
	value = value >= 1.0f ? 1.0f - d : (value <= -1.0f ? -1.0f + d : value);

	int w = tex->Width - 1;
	unsigned char packed = (value * 128) + 128;//((value * 127) + 128) - 1;

	//std::cout << coord << " --> <" << ((int)(w * coord.x)) << ", " << ((int)(w * coord.y)) << ", " << ((int)(w * coord.z)) << "> == " << value << " " << (int)packed<<"\n";
	tex->Voxel[(int)(w * coord.x)][(int)(w * coord.y)][(int)(w * coord.z)] = (unsigned char)packed;
}

void IncrementTexture3D(Volume* voxel, Texture3D* tex, vec3 coord, float value){
	float current = SampleTexture3D(voxel, tex, coord) + value;
	UpdateTexture3D(voxel, tex, coord, current);

}

Texture3D* CreateTexture3D(int resolution){
	int width2D = resolution;

		// Create the 3D texture (note all must be cubes)
		Texture3D temp = {
			width2D,
			width2D,
			width2D,
			NULL
		};

		temp.Voxel = new unsigned char**[width2D];
		for(int i = 0; i < width2D; i++){
			temp.Voxel[i] = new unsigned char*[width2D];

			for(int j = 0; j < width2D; j++)
				temp.Voxel[i][j] = new unsigned char[width2D];
		}
		std::cout << "Memory allocated\n" << "Width: " << width2D << "\nHeight: " << width2D << "\n";

		for(int z = 0; z < width2D; z++)
			for(int y = 0; y < width2D; y++)
				for(int x = 0; x < width2D; x++){
					temp.Voxel[x][y][z] = 0; // all zeros
				}
		printf("Loaded successfully!\n");

		Texture3D* out = new Texture3D();
		memcpy(out, &temp, sizeof(Texture3D));
		return out;
}

Texture3D LoadTexure3D(const char* path){
	int width2D, height2D;
	bool hasAlpha;
	GLubyte* data2D;

	if(ImageLoader::loadPngImage(path, width2D, height2D, hasAlpha, &data2D) != 0){
		// loading was successful

		// Create the 3D texture (note all must be cubes)
		Texture3D out = {
			height2D,
			height2D,
			height2D,
			NULL
		};
		out.Voxel = new unsigned char**[height2D];
		for(int i = 0; i < height2D; i++){
			out.Voxel[i] = new unsigned char*[height2D];

			for(int j = 0; j < height2D; j++)
				out.Voxel[i][j] = new unsigned char[height2D];
		}
		//std::cout << "Memory allocated\n" << "Width: " << width2D << "\nHeight: " << height2D << "\n";
		// copy the pixel data into the 3d texture
		int dz = height2D * height2D; // pixels per z layer
		int dy = height2D;          // pixels per y layer
		int stride = hasAlpha ? 4 : 3;
		for(int z = 0; z < height2D; z++)
			for(int y = 0; y < height2D; y++)
				for(int x = 0; x < height2D; x++){
					int i = x + (y * dy) + (z * dz);
					GLubyte* pixel = (data2D + i * stride);

					// average for 'grey scale' values
					for(int j = 0; j < 3; j++){
						//printf("%d ", pixel[j]);
						out.Voxel[x][y][z] = pixel[j];
					}//printf("\n");
					//out.Voxel[x][y][z] /= 3;
				}
		//printf("Loaded successfully!\n");
		return out;
	}
	else{
		Texture3D err = {0};
		return err;
	}


}

#endif